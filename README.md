# crux-spring-cloud-project

## crux-zookeeper-config

>>>

### **crux-zookeeper-config-spring-cloud-starter** vs. **spring-cloud-starter-zookeeper-config**

> The table below summarizes the features that are supported by  **crux-zookeeper-config-spring-cloud-starter** and  **spring-cloud-starter-zookeeper-config**.

|   | crux-zookeeper-config-spring-cloud-starter  | spring-cloud-starter-zookeeper-config  |
| --- | :---: | :---: |
| Property files | application.properties/yml or bootstrap.properties/yml | bootstrap.properties/yml | 
| Scope | Bean | Application |  
| Reload | Y | Y |
| Custom value setter | Y | N |
| Style | [Spring Boot Relaxed Binding 2.0]( https://github.com/spring-projects/spring-boot/wiki/Relaxed-Binding-2.0 ) | Path as name and data as value |
| Binding Support | @ZookeeperConfigProperties or ZookeeperConfigBindConfigurer | @ConfigurationProperties |
| @Value Inject Support | N | Y |
| Validation Support | N | Y |
| Random values | Y | Y |
| Refer back scope | Application & Target configuration source | Application ( Contains all configuration sources )  |

>>>

### 1. Quick Start

#### 1.1 Add maven dependency

```xml
<dependency>
    <groupId>ren.crux</groupId>
    <artifactId>crux-zookeeper-config-spring-cloud-starter</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

#### 1.2 Usage

##### 1.2.1 Using the `@ZookeeperConfigProperties`

```java
@Data
@ZookeeperConfigurationProperties(path = "/foo")
@ConfigurationProperties(prefix = "foo") // optional, using the `@ConfigurationProperties` to config prefix and more
public class FooProperties {
    private String str;
    private long lon;
    private Map<String, String> strMap;
    private Map<String, Bar> barMap;
    private List<String> list;
}
```

##### 1.2.2 Using the `ZookeeperConfigBindConfigurer`

> This way is suitable for existing classes ( dependency project or jar lib) and there is no use @ ConfigurationProperties or @ ZookeeperConfigurationProperties

```java
@Configuration
public static class Config {
    @Bean
    public ZookeeperConfigBindConfigurer barConfig() {
    return binder -> binder.path("/bar")
            .contentType(ConfigurationContentType.Yaml)
            .addBean(BarYaml.class)
            .autoReload();
    }
}
```

#### 1.3 Configure it in zookeeper

> As shown in the following example:
 
```properties
# /config/foo

foo.lon=${server.port:2134}
foo.str=${random.value}
foo.list=a,b,c,d,142
foo.str-map.k1=${random.uuid}
foo.str-map.k2=${spring.application.name:application}
foo.bar-map.bk1.bar-int=${random.int(10)}
foo.bar-map.bk1.bar-str=a
foo.bar-map.bk2.bar-int=${random.int[1024,65536]}
foo.bar-map.bk2.bar-str=${foo.bar-map.bk1.bar-str}
```
 
### 2. Options

```properties
# Global zookeeper config switch
crux.cloud.zookeeper.config.enabled=true
# Root folder where the configuration for Zookeeper is kept
crux.cloud.zookeeper.config.root=/config
# Throw exceptions during config lookup if true, otherwise, log warnings.
crux.cloud.zookeeper.config.fail-fast=true
# Create parent folders ( If it not exist )
crux.cloud.zookeeper.config.create-containers=true
```

### 3. Custom value setter

> If you want to select whether to update, you can implement `ZookeeperConfigurationChangeListener` and return true or false in the `onChange` function

eg. 
```java
@Data
@ZookeeperConfigurationProperties(path = "/bar", contentType = ConfigurationContentType.Yaml)
public class BarYaml implements ZookeeperConfigurationChangeListener<BarYaml> {

    private String value = "v1";
    private int version = 0;

    @Override
    public boolean onChange(BarYaml newValue) {
        return newValue.getVersion() > version;
    }
}
```

### 4. Stateful Object

> If your configuration used to an stateful object, you can implement `ZookeeperConfigurationRefreshListener` and update the stateful object in the `onRefresh` function

eg.
```java
@Data
@ZookeeperConfigurationProperties(path = "/foo")
public class FooProperties implements ZookeeperConfigurationRefreshListener {
    
    private double permitsPerSecond = 100;
    private RateLimiter limiter = RateLimiter.create(permitsPerSecond);

    @Override
    public void onRefresh() {
        limiter.setRate(permitsPerSecond);
    }
}
```
