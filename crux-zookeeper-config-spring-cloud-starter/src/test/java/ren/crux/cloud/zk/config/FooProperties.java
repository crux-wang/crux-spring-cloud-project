/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.cloud.zk.config;

import com.google.common.util.concurrent.RateLimiter;
import lombok.Data;
import ren.crux.cloud.zk.config.listener.ZookeeperConfigurationRefreshListener;

import java.util.List;
import java.util.Map;

/**
 * @author wangzhihui
 */
@Data
public class FooProperties implements ZookeeperConfigurationRefreshListener {

    private String str;
    private long lon;
    private Map<String, String> strMap;
    private Map<String, Bar> barMap;
    private List<String> list;
    private double permitsPerSecond = 100;
    private RateLimiter limiter = RateLimiter.create(permitsPerSecond);

    @Override
    public void onRefresh() {
        limiter.setRate(permitsPerSecond);
    }
}
