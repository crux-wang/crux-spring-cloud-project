/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.cloud.zk.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.test.TestingServer;
import org.apache.zookeeper.data.Stat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;
import ren.crux.common.CommonAutoConfiguration;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author wangzhihui
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "lon=123123")
@Import({CommonAutoConfiguration.class, ZookeeperConfigAutoConfiguration.class})
@AutoConfigureAfter(ZookeeperConfigurationRegistryTest.Config.class)
@ComponentScan(basePackageClasses = FooProperties.class)
public class ZookeeperConfigurationRegistryTest {
    @Configuration
    public static class Config {
        private ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3);

        @Bean(destroyMethod = "close")
        public TestingServer server() throws Exception {
            TestingServer testingServer = new TestingServer();
            testingServer.start();
            return testingServer;
        }

        @Bean(destroyMethod = "close")
        public CuratorFramework curatorFramework(TestingServer server) throws Exception {
            CuratorFramework curatorFramework = CuratorFrameworkFactory.builder()
                    .connectString(server.getConnectString())
                    .retryPolicy(retryPolicy)
                    .build();
            curatorFramework.start();
            return curatorFramework;
        }

        @Bean
        public ZookeeperConfigBindConfigurer barConfig() {
            return binder -> binder.path("/bar")
                    .contentType(ConfigurationContentType.Yaml)
                    .addBean(BarYaml.class)
                    .autoReload();
        }

        @Bean
        public ZookeeperConfigBindConfigurer fooConfig() {
            return binder -> binder.path("/foo")
                    .addBean(FooProperties.class);
        }

        @Bean
        public FooProperties fooProperties() {
            return new FooProperties();
        }
    }

    @Autowired
    private ZookeeperConfigurationRegistry registry;

    @Autowired
    private FooProperties fooProperties;

    @Autowired
    private BarYaml barYaml;

    @Autowired
    private OnceProperties onceProperties;

    @Autowired
    private TestingServer server;

    @Autowired
    private CuratorFramework curatorFramework;

    @Before
    public void setUp() throws Exception {
        log.info("connect string : {}", server.getConnectString());
        assertNotNull(fooProperties);
        assertNotNull(registry);
    }

    @Test
    public void registry() {
        log.info("Subscriber Group : {}", registry.getSubscriberGroup());
        assertEquals(new HashSet<>(Arrays.asList("/config/foo", "/config/bar")), registry.getAutoReloadPaths());
        Set<ZookeeperConfigBinder> subscribers = registry.getSubscribers("/config/foo");
        assertEquals(1, subscribers.size());
        ZookeeperConfigBinder fooBinder = new ZookeeperConfigBinder();
        fooBinder.setAutoReload(true);
        fooBinder.setRelative(true);
        fooBinder.setPath("/foo");
        fooBinder.addBean(FooProperties.class);
        fooBinder.setContentType(ConfigurationContentType.Properties);
        assertEquals(Collections.singleton(fooBinder), subscribers);

        subscribers = registry.getSubscribers("/config/bar");
        assertEquals(1, subscribers.size());
        ZookeeperConfigBinder barBinder = new ZookeeperConfigBinder();
        barBinder.setAutoReload(true);
        barBinder.setRelative(true);
        barBinder.setPath("/bar");
        barBinder.addBean(BarYaml.class);
        barBinder.setContentType(ConfigurationContentType.Yaml);
        assertEquals(Collections.singleton(barBinder), subscribers);

        subscribers = registry.getSubscribers("/config/once");
        assertEquals(1, subscribers.size());
        ZookeeperConfigBinder onceBinder = new ZookeeperConfigBinder();
        onceBinder.setAutoReload(false);
        onceBinder.setRelative(true);
        onceBinder.setPath("/once");
        onceBinder.addBean(OnceProperties.class);
        onceBinder.setContentType(ConfigurationContentType.Properties);
        assertEquals(Collections.singleton(onceBinder), subscribers);

    }

    @Test
    public void propertiesTest() throws Exception {
        log.info("Foo properties : {}", fooProperties);
        Stat stat = curatorFramework.checkExists().forPath("/config/foo");
        assertNotNull(stat);
        Properties source = PropertiesLoaderUtils.loadAllProperties("text.properties");
        assertNotNull(source);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        source.store(os, "");
        stat = curatorFramework.setData().forPath("/config/foo", os.toByteArray());
        assertNotNull(stat);
        log.info("content : {}", new String(curatorFramework.getData().forPath("/config/foo")));
        Thread.sleep(100);
        log.info("Foo properties : {}", fooProperties);
        assertConfiguration(fooProperties);
    }

    @Test
    public void onceTest() throws Exception {
        log.info("Once properties : {}", onceProperties);
        String expected = onceProperties.getStr();
        Stat stat = curatorFramework.checkExists().forPath("/config/once");
        assertNotNull(stat);
        stat = curatorFramework.setData().forPath("/config/once", ("once.str=" + System.currentTimeMillis()).getBytes());
        assertNotNull(stat);
        log.info("content : {}", new String(curatorFramework.getData().forPath("/config/once")));
        Thread.sleep(100);
        log.info("Once properties : {}", onceProperties);
        assertEquals(expected, onceProperties.getStr());
    }

    @Test
    public void yamlTest() throws Exception {
        log.info("Bar yaml : {}", barYaml);
        assertEquals(new BarYaml(), barYaml);
        Stat stat = curatorFramework.checkExists().forPath("/config/bar");
        assertNotNull(stat);
        URL url = ResourceUtils.getURL("src/test/resources/text.yml");
        byte[] bytes = IOUtils.toByteArray(url);
        assertNotNull(bytes);
        stat = curatorFramework.setData().forPath("/config/bar", bytes);
        assertNotNull(stat);
        log.info("content : {}", new String(curatorFramework.getData().forPath("/config/bar")));
        Thread.sleep(100);
        log.info("Bar yaml : {}", barYaml);
        assertConfiguration(barYaml);
    }

    public void assertConfiguration(FooProperties foo) {
        assertEquals(123123L, foo.getLon());
        assertNotNull(foo.getStr());
        assertEquals(Arrays.asList("a", "b", "c", "d", "142"), foo.getList());
        Map<String, String> strMap = new HashMap<>();
        strMap.put("k1", "v1");
        strMap.put("k2", "v2");
        assertEquals(strMap, foo.getStrMap());
        Map<String, Bar> barMap = new HashMap<>();
        barMap.put("bk1", new Bar(1, "a"));
        barMap.put("bk2", new Bar(1, "123"));
        assertEquals(barMap, foo.getBarMap());
    }

    public void assertConfiguration(BarYaml bar) {
        assertEquals(123123L, bar.getLon());
        assertNotNull(bar.getStr());
        assertEquals(Arrays.asList("a", "b", "c", "d", "142"), bar.getList());
        Map<String, String> strMap = new HashMap<>();
        strMap.put("k1", "v1");
        strMap.put("k2", "v2");
        assertEquals(strMap, bar.getStrMap());
        Map<String, Bar> barMap = new HashMap<>();
        barMap.put("bk1", new Bar(1, "a"));
        barMap.put("bk2", new Bar(1, "123"));
        assertEquals(barMap, bar.getBarMap());
    }
}