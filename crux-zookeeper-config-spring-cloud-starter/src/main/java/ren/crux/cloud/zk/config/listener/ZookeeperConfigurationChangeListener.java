/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.cloud.zk.config.listener;

/**
 * Used to customize set values when configuration changes
 *
 * @author wangzhihui
 * @see ren.crux.cloud.zk.config.ZookeeperConfigurationProperties
 * @see ZookeeperConfigurationRefreshListener
 */
public interface ZookeeperConfigurationChangeListener<T> {

    /**
     * when configuration changes
     *
     * @param newValue The new value
     * @return {@code false} to stop default setter
     */
    boolean onChange(T newValue);

}
