/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.cloud.zk.config;

import com.google.common.collect.HashMultimap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author wangzhihui
 */
@Slf4j
public class ZookeeperConfigurationRegistry {

    private final ZookeeperConfigProperties properties;
    private final HashMultimap<String, ZookeeperConfigBinder> subscriberGroup = HashMultimap.create();
    private final List<ZookeeperConfigBindConfigurer> configurers = new ArrayList<>();
    private final ApplicationContext applicationContext;
    private final Map<String, ConfigurationContentType> path2ContentType = new ConcurrentHashMap<>();
    private final Set<String> autoReloadPaths = new HashSet<>();

    public ZookeeperConfigurationRegistry(ApplicationContext applicationContext, List<ZookeeperConfigBindConfigurer> configurers, ZookeeperConfigProperties properties) {
        this.applicationContext = applicationContext;
        this.properties = properties;
        if (configurers != null) {
            this.configurers.addAll(configurers);
        }
    }

    @PostConstruct
    private void init() {
        Map<String, Object> configurations = applicationContext.getBeansWithAnnotation(ZookeeperConfigurationProperties.class);
        configurations.forEach((beanName, bean) -> {
            Class<?> beanClass = bean.getClass();
            ZookeeperConfigurationProperties zkConfig = AnnotationUtils.findAnnotation(beanClass, ZookeeperConfigurationProperties.class);
            if (zkConfig != null) {
                ZookeeperConfigBinder binder = new ZookeeperConfigBinder();
                binder.addBean(beanClass);
                binder.contentType(zkConfig.contentType());
                binder.setAutoReload(zkConfig.autoReload());
                binder.setPath(zkConfig.path());
                binder.setRelative(zkConfig.relative());
                register(binder);
            }
        });
        configurers.forEach(configurer -> {
            ZookeeperConfigBinder binder = new ZookeeperConfigBinder();
            configurer.configure(binder);
            register(binder);
        });
    }

    public void register(ZookeeperConfigBinder binder) {
        String path = binder.getPath();
        if (binder.isRelative()) {
            path = properties.getRoot() + path;
        }
        log.debug("Register zookeeper config binder : {} - {}", path, binder);
        subscriberGroup.put(path, binder);
        ConfigurationContentType contentType = path2ContentType.putIfAbsent(path, binder.getContentType());
        if (contentType != null && contentType != binder.getContentType()) {
            if (this.properties.isFailFast()) {
                ReflectionUtils.rethrowRuntimeException(new IllegalArgumentException("invalid zookeeper configuration content type, path :" + path + ", " + contentType + " or " + binder.getContentType() + " ?"));
            } else {
                log.error("Invalid zookeeper configuration content type, path : {}, {} or {} ?", path, contentType, binder.getContentType());
            }
        }
        if (binder.isAutoReload()) {
            autoReloadPaths.add(path);
        }
    }

    public Set<ZookeeperConfigBinder> getSubscribers(String path) {
        return subscriberGroup.get(path);
    }

    public HashMultimap<String, ZookeeperConfigBinder> getSubscriberGroup() {
        return subscriberGroup;
    }

    public Set<String> paths() {
        return subscriberGroup.keySet();
    }

    public Map<String, ConfigurationContentType> getPath2ContentType() {
        return path2ContentType;
    }

    public ConfigurationContentType getContentType(String path) {
        return path2ContentType.get(path);
    }

    public Set<String> getAutoReloadPaths() {
        return autoReloadPaths;
    }

    public boolean isAutoReload(String path) {
        return autoReloadPaths.contains(path);
    }
}
