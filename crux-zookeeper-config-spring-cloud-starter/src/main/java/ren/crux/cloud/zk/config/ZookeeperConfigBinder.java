/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.cloud.zk.config;

import lombok.Data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author wangzhihui
 */
@Data
public class ZookeeperConfigBinder {

    private Set<Class<?>> beanClasses = new HashSet<>();
    private String path;
    private boolean autoReload = true;
    private boolean relative = true;
    private ConfigurationContentType contentType = ConfigurationContentType.Properties;

    public ZookeeperConfigBinder addBean(Class<?>... beanClasses) {
        this.beanClasses.addAll(Arrays.asList(beanClasses));
        return this;
    }

    public ZookeeperConfigBinder contentType(ConfigurationContentType contentType) {
        this.contentType = contentType;
        return this;
    }

    public ZookeeperConfigBinder relative() {
        this.relative = true;
        return this;
    }

    public ZookeeperConfigBinder absolute() {
        this.relative = false;
        return this;
    }

    public ZookeeperConfigBinder path(String path) {
        this.path = path;
        return this;
    }

    public ZookeeperConfigBinder autoReload() {
        this.autoReload = true;
        return this;
    }

    public ZookeeperConfigBinder disableAutoReload() {
        this.autoReload = false;
        return this;
    }
}
