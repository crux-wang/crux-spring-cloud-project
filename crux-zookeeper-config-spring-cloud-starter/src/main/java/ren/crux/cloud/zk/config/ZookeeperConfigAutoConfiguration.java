/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.cloud.zk.config;


import org.apache.curator.framework.CuratorFramework;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.zookeeper.ConditionalOnZookeeperEnabled;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ren.crux.cloud.zk.config.endpoint.ZookeeperConfigEndpoint;
import ren.crux.common.util.ConfigurationParser;

import java.util.List;

/**
 * @author wangzhihui
 */
@Configuration
@ConditionalOnZookeeperEnabled
@EnableConfigurationProperties(ZookeeperConfigProperties.class)
@ConditionalOnProperty(prefix = "crux.cloud.zookeeper.config", name = "enabled", matchIfMissing = true)
public class ZookeeperConfigAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public ZookeeperConfigurationRegistry zookeeperConfigurationRegistry(ApplicationContext context, ZookeeperConfigProperties properties, List<ZookeeperConfigBindConfigurer> configurers) {
        return new ZookeeperConfigurationRegistry(context, configurers, properties);
    }

    @Bean
    @ConditionalOnMissingBean
    public ZookeeperConfigurationService zookeeperConfigurationService(ApplicationContext context, ApplicationEventPublisher publisher,
                                                                  CuratorFramework curator, ConfigurationParser configurationParser,
                                                                  ZookeeperConfigProperties properties,
                                                                  ZookeeperConfigurationRegistry registry) {
        return new ZookeeperConfigurationService(context, publisher, configurationParser, curator, properties, registry);
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(Endpoint.class)
    public ZookeeperConfigEndpoint zookeeperConfigEndpoint(ApplicationContext context,
                                                           ZookeeperConfigurationRegistry registry) {
        return new ZookeeperConfigEndpoint(context, registry);
    }

}
