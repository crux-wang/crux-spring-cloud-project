/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.cloud.zk.config.endpoint;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.context.ApplicationContext;
import ren.crux.cloud.zk.config.ZookeeperConfigBinder;
import ren.crux.cloud.zk.config.ZookeeperConfigurationRegistry;

import java.util.*;

/**
 * @author wangzhihui
 */
@Endpoint(id = "zookeeperconfigurations")
public class ZookeeperConfigEndpoint {

    private final ApplicationContext context;
    private final ZookeeperConfigurationRegistry registry;

    public ZookeeperConfigEndpoint(ApplicationContext context, ZookeeperConfigurationRegistry registry) {
        this.context = context;
        this.registry = registry;
    }

    @ReadOperation
    public Map<String, List<Pair<String, Object>>> configurations() {
        Map<String, Collection<ZookeeperConfigBinder>> subscribersMap = registry.getSubscriberGroup().asMap();
        Map<String, List<Pair<String, Object>>> result = new HashMap<>(16);
        subscribersMap.forEach((path, subscribers) -> {
            List<Pair<String, Object>> items = new ArrayList<>();
            subscribers.forEach(subscriber ->
                    subscriber.getBeanClasses().forEach(clazz -> items.add(Pair.of(clazz.getName(), context.getBean(clazz))))
            );
            result.put(path, items);
        });
        return result;
    }
}
