/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.cloud.zk.config;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.api.CuratorWatcher;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.Watcher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.ReflectionUtils;
import ren.crux.cloud.zk.config.event.ZookeeperConfigChangedEvent;
import ren.crux.cloud.zk.config.listener.ZookeeperConfigurationChangeListener;
import ren.crux.cloud.zk.config.listener.ZookeeperConfigurationRefreshListener;
import ren.crux.common.CommonConstants;
import ren.crux.common.util.ConfigurationParser;

import javax.annotation.PostConstruct;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

/**
 * @author wangzhihui
 */
@Slf4j
public class ZookeeperConfigurationService {

    private final CuratorFramework curator;
    private final ZookeeperConfigProperties zookeeperConfigProperties;
    private final ConfigurationParser configurationParser;
    private final ApplicationContext context;
    private final ApplicationEventPublisher publisher;
    private final ZookeeperConfigurationRegistry registry;

    public ZookeeperConfigurationService(ApplicationContext context, ApplicationEventPublisher publisher,
                                         ConfigurationParser configurationParser, CuratorFramework curator,
                                         ZookeeperConfigProperties zookeeperConfigProperties,
                                         ZookeeperConfigurationRegistry registry) {
        this.context = context;
        this.publisher = publisher;
        this.curator = curator;
        this.zookeeperConfigProperties = zookeeperConfigProperties;
        this.configurationParser = configurationParser;
        this.registry = registry;
    }

    @PostConstruct
    private void init() {
        registry.paths().forEach(path ->
                onChanged(path, registry.getContentType(path), registry.isAutoReload(path), true)
        );
    }

    private void onChanged(@NonNull String path) {
        onChanged(path, registry.getContentType(path), registry.isAutoReload(path), false);
    }

    @SuppressWarnings("unchecked")
    private void onChanged(@NonNull String path, ConfigurationContentType contentType, boolean autoReload, boolean onInit) {
        byte[] bytes = getContentBytes(path, zookeeperConfigProperties.isCreateContainers(), autoReload);
        if (bytes != null && bytes.length > 0) {
            ZookeeperConfigChangedEvent event = new ZookeeperConfigChangedEvent(curator, path, new String(bytes, CommonConstants.UTF_8_CHARSET));
            Optional<Properties> optional = getProperties(bytes, contentType);
            optional.ifPresent(
                    properties -> {
                        Set<ZookeeperConfigBinder> subscribers = registry.getSubscribers(path);
                        subscribers.forEach(
                                subscriber -> subscriber.getBeanClasses().forEach(clazz -> {
                                    Object bean = context.getBean(clazz);
                                    if (bean != null) {
                                        if (bean instanceof ZookeeperConfigurationChangeListener) {
                                            if (!((ZookeeperConfigurationChangeListener) bean).onChange(configurationParser.parse(clazz, properties).get())) {
                                                return;
                                            }
                                        }
                                        try {
                                            configurationParser.bind(bean, properties).orElseThrow(NoSuchElementException::new);
                                            if (bean instanceof ZookeeperConfigurationRefreshListener) {
                                                ((ZookeeperConfigurationRefreshListener) bean).onRefresh();
                                            }
                                        } catch (Exception e) {
                                            if (onInit && zookeeperConfigProperties.isFailFast()) {
                                                ReflectionUtils.rethrowRuntimeException(e);
                                            } else {
                                                log.error("Unable to load zookeeper config from {} to {}", path, clazz, e);
                                            }
                                        }
                                    } else {
                                        log.warn("No such bean class {}", clazz);
                                    }
                                })
                        );
                        publisher.publishEvent(event);
                    }
            );
        }
    }

    private Optional<Properties> getProperties(byte[] bytes, ConfigurationContentType contentType) {
        switch (contentType) {
            case Yaml:
                return Optional.of(configurationParser.yamlBytes2Properties(bytes));
            case Properties:
            default:
                try {
                    return Optional.of(configurationParser.bytes2Properties(bytes));
                } catch (Exception e) {
                    log.error("Zookeeper config load bytes err, data :{}",
                            new String(bytes, CommonConstants.UTF_8_CHARSET), e);
                }
        }
        return Optional.empty();
    }

    private byte[] getContentBytes(String fullPath, boolean createContainers, boolean autoReload) {
        try {
            byte[] bytes = null;
            try {
                if (createContainers) {
                    this.curator.createContainers(fullPath);
                }
                if (autoReload) {
                    bytes = this.curator.getData().usingWatcher((CuratorWatcher) event -> {
                        log.info("On zookeeper config event : {}", event);
                        if (event.getType() == Watcher.Event.EventType.NodeDataChanged) {
                            onChanged(event.getPath());
                        }
                    }).forPath(fullPath);
                } else {
                    bytes = this.curator.getData().forPath(fullPath);
                }
            } catch (KeeperException e) {
                // not found
                if (e.code() != KeeperException.Code.NONODE) {
                    throw e;
                }
            }
            return bytes;
        } catch (Exception exception) {
            ReflectionUtils.rethrowRuntimeException(exception);
        }
        return null;
    }

}
