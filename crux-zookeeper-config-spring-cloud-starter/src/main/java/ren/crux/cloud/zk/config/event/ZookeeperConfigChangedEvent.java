/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.cloud.zk.config.event;

import lombok.Getter;
import org.apache.curator.framework.CuratorFramework;
import org.springframework.context.ApplicationEvent;

/**
 * @author wangzhihui
 */
@Getter
public class ZookeeperConfigChangedEvent extends ApplicationEvent {

    private final String path;
    private final String content;

    public ZookeeperConfigChangedEvent(CuratorFramework source, String path, String content) {
        super(source);
        this.path = path;
        this.content = content;
    }

    public CuratorFramework getCuratorFramework(){
        return (CuratorFramework) source;
    }
}
